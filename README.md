
## Configuração

Caso deseje alterar a configuração padrão da aplicação, como porta do serviço, ou configuraçōes do banco de dados
acesse o arquivo de configuração em src/main/resource/appication.properties. Porém esta ação não é necessária para
execução do projeto, somente se já houver outro serviço utilizando a porta 8081, neste caso será necessário alterar
esta porta para uma porta que esteja disponível.

---

## Execução

Para executar o projeto é necessário que o arquivo movielist.csv esteja no diretório raiz do projeto, e que tenha o
maven instalado na máquina, e que o projeto esteja compilado. com estes requisitos atendidos basta executar o seguinte
comando no diretório raiz do projeto:

	mvn spring-boot:run

Outra forma de execução do projeto é através da arquivo compilado .war, neste caso basta copiar este arquivo para o
diretório de deploy que dependerá do servidor de aplicação utilizado (Apache, JBoss, WildFly, etc.).

---

## Execução dos testes de integração

Ao compilar o projeto será executado os testes de integração, mas caso queira executar somente os testes de integração
basta executar o seguinte comando no diretório raiz do projeto:

	mvn test
	
O projeto foi desenvolvido no netbeans, então basta clonar o projeto, abrir o projeto no netbeans, clicar sobre o projeto
e depois na opção "limpar e construir" ou "Construir com dependências", assim o projeto será compilado e após a compilação
será executado os testes unitários.

## Banco de dados

Neste projeto foi utilizado o banco de dados H2, após a aplicação ser inicializada, caso deseje visualizar os dados
salvos no banco de dados, basta acessar o link abaixo:

	localhost:8081/h2-ui
	
Ao acessar bastar clicar em Connect, por default não foi setado nenhuma senha para o acesso.
