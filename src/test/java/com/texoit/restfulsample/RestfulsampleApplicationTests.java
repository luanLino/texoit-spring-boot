package com.texoit.restfulsample;

import com.texoit.restfulsample.controller.IndicatedMovieController;
import com.texoit.restfulsample.helper.CSVHelper;
import com.texoit.restfulsample.model.IndicatedMovie;
import com.texoit.restfulsample.model.Producer;
import com.texoit.restfulsample.model.Studio;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import static org.junit.Assert.assertTrue;  

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
class RestfulsampleApplicationTests {
    
    @Autowired
    private IndicatedMovieController restfulController;
    
    private static long indicatedMovieId;

	@Test
        @Order(1)
	void contextLoads() {
            RestfulsampleApplication.main(new String[0]);
	}
        
        @Test
        @Order(2)        
        void testGetAllIndicatedMovie() throws FileNotFoundException{
                        
            ResponseEntity<List<IndicatedMovie>> response = restfulController.getAllIndicated(null);
                        
            assertTrue(response!=null);
            
            assertTrue(response.getStatusCodeValue() == 200);
                                    
            List<IndicatedMovie> indicatedsFromApi = response.getBody();
            
            assertTrue(indicatedsFromApi!=null);
                        
            InputStream csvDataBase = new FileInputStream("movielist.csv");
            
            assertTrue(csvDataBase != null);
            
            List<IndicatedMovie> indicatedsFromCsv = CSVHelper.csvToIndicated(csvDataBase);
            
            assertTrue(indicatedsFromCsv != null);
            
            //assertTrue(indicatedsFromApi.containsAll(indicatedsFromCsv));
            
            indicatedMovieId = indicatedsFromApi.get(0).getId();
            
        }
        
        
        @Test
        @Order(3)
        void testGetIndicatedMovieById() throws FileNotFoundException{
            
            ResponseEntity<IndicatedMovie> response = restfulController.getIndicatedById(indicatedMovieId);

            assertTrue(response!=null);
            System.out.println("status code:"+response.getStatusCodeValue());
            assertTrue(response.getStatusCodeValue() == 200);
            
            //test invalid id
            response = restfulController.getIndicatedById(213482734);

            assertTrue(response!=null);
            
            assertTrue(response.getStatusCodeValue() == 404);
            
        }
        
        @Test
        @Order(4)
        void testPostCreateIndicatedMovie() throws FileNotFoundException{
            
            IndicatedMovie movie = new IndicatedMovie();
            
            Set<Producer> producers = new HashSet<Producer>();
            Set<Studio> studios = new HashSet<Studio>();
            
            movie.setTitle("Movie from unit test");
            movie.setWinner(false);
            movie.setYear(2022);
            movie.setProducers(producers);
            movie.setStudios(studios);
            
            ResponseEntity<IndicatedMovie> response = restfulController.createIndicated(movie);
            
            assertTrue(response!=null);
                                    
            assertTrue(response.getStatusCodeValue() == 201);
            
            ResponseEntity<IndicatedMovie> response2 = restfulController.getIndicatedById(response.getBody().getId());
            
            assertTrue(response2!=null);
            
            assertTrue(response2.getStatusCodeValue() == 200);
            
            assertEquals(response.getBody(),response2.getBody());
            
            indicatedMovieId = response.getBody().getId();
            
        }
        
        @Test
        @Order(5)
        void testPutUpdateIndicatedMovie() throws FileNotFoundException{
            
           IndicatedMovie movie = new IndicatedMovie();
            
           movie.setTitle("Movie from unit test updated");
           movie.setWinner(true);
           movie.setYear(2021);
            
           ResponseEntity<IndicatedMovie> response = restfulController.updateIndicated(indicatedMovieId, movie);

           assertTrue(response!=null);
            
           assertTrue(response.getStatusCodeValue() == 200);
           
           ResponseEntity<IndicatedMovie> response2 = restfulController.getIndicatedById(indicatedMovieId);
            
           assertTrue(response2!=null);
            
           assertTrue(response2.getStatusCodeValue() == 200);
           
           assertEquals(response2.getBody().getTitle(), "Movie from unit test updated");
           
           assertEquals(response2.getBody().isWinner(), true);
           
           assertEquals(response2.getBody().getYear(), 2021);
           
            
        }
        
        
        @Test
        @Order(6)
        void testPatchUpdateIndicatedMovie() throws FileNotFoundException{
         
           IndicatedMovie movie = new IndicatedMovie();
            
           movie.setTitle("Movie from unit test patch");
           movie.setWinner(false);
            
           ResponseEntity<IndicatedMovie> response = restfulController.patchIndicated(indicatedMovieId, movie);

           assertTrue(response!=null);
            
           assertTrue(response.getStatusCodeValue() == 200);
           
           ResponseEntity<IndicatedMovie> response2 = restfulController.getIndicatedById(indicatedMovieId);
            
           assertTrue(response2!=null);
            
           assertTrue(response2.getStatusCodeValue() == 200);
           
           assertEquals(response2.getBody().getTitle(), "Movie from unit test patch");
           
           assertEquals(response2.getBody().isWinner(), false);
           
           assertEquals(response2.getBody().getYear(), 2021);
            
        }
        
        
        @Test
        @Order(7)
        void testDeleteIndicatedMovie() throws FileNotFoundException{

           ResponseEntity<HttpStatus> response = restfulController.deleteIndicated(indicatedMovieId);

           assertTrue(response!=null);
            
           assertTrue(response.getStatusCodeValue() == 204);
           
           assertTrue(!response.hasBody());
           
           ResponseEntity<IndicatedMovie> response2 = restfulController.getIndicatedById(indicatedMovieId);
            
           assertTrue(response2!=null);
            
           assertTrue(response2.getStatusCodeValue() == 404);
            
        }
        
        @Test
        @Order(8)
        void testGetIndicatedMovieMinMaxInterval() throws FileNotFoundException{
            
           Map<String, Object> map = new HashMap<String, Object>();
            
           ResponseEntity<Object> response = restfulController.findByProducersInterval();

           assertTrue(response!=null);
            
           assertTrue(response.getStatusCodeValue() == 200);
           
           map = (Map<String, Object>) response.getBody();
           
           assertThat(map != null);
           
           assertTrue(!map.isEmpty());
           
           assertTrue((map.get("min") != null));
           
           assertTrue((map.get("max") != null));
            
        }
        
        
           

}
