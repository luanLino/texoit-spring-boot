/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.texoit.restfulsample.repository;

import com.texoit.restfulsample.model.Producer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 *
 * @author luan
 */
@Component
public interface ProducerRepository extends JpaRepository<Producer, Long> {
  Producer findByName(String name);

}
