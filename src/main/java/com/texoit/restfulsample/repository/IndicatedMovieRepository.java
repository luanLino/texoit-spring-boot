/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.texoit.restfulsample.repository;

import java.util.List;
import com.texoit.restfulsample.model.IndicatedMovie;
import com.texoit.restfulsample.pojo.WinIntervalResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

/**
 *
 * @author luan
 */
@Component
public interface IndicatedMovieRepository extends JpaRepository<IndicatedMovie, Long> {
  List<IndicatedMovie> findByWinner(boolean winner);
  List<IndicatedMovie> findByTitleContaining(String title);
  
  
  //@Query(nativeQuery = true, value = "select producer.name as producerName ,min(movie.year) as previousWin, max(movie.year) as followingWin, (max(movie.year) - min(movie.year) ) as intervall  from movie,producer,movie_producers where movie.id=movie_producers.indicated_movie_id and producer.id=movie_producers.producers_id and winner=true group by producer.name having count(movie.winner) > 1")
  @Query(nativeQuery = false, value = "select new com.texoit.restfulsample.pojo.WinIntervalResult(producer.name,min(movie.year), max(movie.year), (max(movie.year) - min(movie.year)) ) from com.texoit.restfulsample.model.IndicatedMovie movie, com.texoit.restfulsample.model.Producer producer, com.texoit.restfulsample.model.IndicatedMovie_Producers movie_producers where movie.id=movie_producers.indicated_movie_id and producer.id=movie_producers.producers_id and winner=true group by producer.name having count(movie.winner) > 1")
  List<WinIntervalResult> findByProducersWithIntervalWin();
//  
//  
//  List<IndicatedMovie> findByMaxInterval();


  //List<Indicated> findByShortestIntervalBetweenAwards();
  //List<Indicated> findByBiggestIntervalBetweenAwards();
}
