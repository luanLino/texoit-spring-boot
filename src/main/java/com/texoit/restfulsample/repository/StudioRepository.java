/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.texoit.restfulsample.repository;

import com.texoit.restfulsample.model.Studio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 *
 * @author luan
 */
@Component
public interface StudioRepository extends JpaRepository<Studio, Long> {
  Studio findByName(String name);

}
