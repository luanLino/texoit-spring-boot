/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.texoit.restfulsample.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.texoit.restfulsample.model.IndicatedMovie;
import com.texoit.restfulsample.model.Producer;
import com.texoit.restfulsample.model.Studio;
import com.texoit.restfulsample.pojo.WinIntervalResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.texoit.restfulsample.repository.IndicatedMovieRepository;
import com.texoit.restfulsample.repository.ProducerRepository;
import com.texoit.restfulsample.repository.StudioRepository;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import org.springframework.web.bind.annotation.PatchMapping;

/**
 *
 * @author luan
 */
@RestController
@RequestMapping("/api")
public class IndicatedMovieController {

    @Autowired
    private IndicatedMovieRepository indicatedlRepository;
    
    @Autowired
    private ProducerRepository producerRepository;
    
    @Autowired
    private StudioRepository studioRepository;
    

    @GetMapping("/indicated-movie")
    public ResponseEntity<List<IndicatedMovie>> getAllIndicated(@RequestParam(required = false) String title) {
        try {
            List<IndicatedMovie> indicated = new ArrayList<IndicatedMovie>();
            if (title == null) {
                indicatedlRepository.findAll().forEach(indicated::add);
            } else {
                indicatedlRepository.findByTitleContaining(title).forEach(indicated::add);
            }
            if (indicated.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(indicated, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/indicated-movie/{id}")
    public ResponseEntity<IndicatedMovie> getIndicatedById(@PathVariable("id") long id) {
        
        Optional<IndicatedMovie> indicatedData = indicatedlRepository.findById(id);
        
        if (indicatedData.isPresent()) {
            return new ResponseEntity<>(indicatedData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/indicated-movie")
    public ResponseEntity<IndicatedMovie> createIndicated(@RequestBody IndicatedMovie indicated) {
        try {
            
            Set<Producer> producers = new HashSet<Producer>();
            Set<Studio> studios = new HashSet<Studio>();
            //List<IndicatedMovie> indicateds = new ArrayList<IndicatedMovie>();

           
            for(Producer producer : indicated.getProducers()){
                
                    String name = producer.getName();
                    
                   Producer producerDatabase = producerRepository.findByName(name);
                   
                   if(producerDatabase==null){
                       producer = new Producer();
                       producer.setName(name);
                       producerRepository.save(producer);
                   }else{
                       producer = producerDatabase;
                   }
                   
                   producers.add(producer);
            }
            
            for(Studio studio : indicated.getStudios()){
                
                    String name = studio.getName();
                    
                   Studio studioDatabase = studioRepository.findByName(name);
                   
                   if(studioDatabase==null){
                       studio = new Studio();
                       studio.setName(name);
                      studio = studioRepository.save(studio);
                   }else{
                       studio = studioDatabase;
                   }
                   
                   studios.add(studio);
            }
            


            IndicatedMovie _indicated = indicatedlRepository//.save(indicated);
                    .save(new IndicatedMovie(indicated.getTitle(),
                              producers,
                              studios,
                              indicated.getYear(),
                              indicated.isWinner()
                    ));

            
            return new ResponseEntity<>(_indicated, HttpStatus.CREATED);
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERROR:"+e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/indicated-movie/{id}")
    public ResponseEntity<IndicatedMovie> updateIndicated(@PathVariable("id") long id, @RequestBody IndicatedMovie indicated) {
       
        Optional<IndicatedMovie> indicatedData = indicatedlRepository.findById(id);
        
        if (indicatedData.isPresent()) {
            IndicatedMovie _indicated = indicatedData.get();
            _indicated.setTitle(indicated.getTitle());
            _indicated.setProducers(indicated.getProducers());
            _indicated.setStudios(indicated.getStudios());
            _indicated.setYear(indicated.getYear());
            _indicated.setWinner(indicated.isWinner());
            
            
            return new ResponseEntity<>(indicatedlRepository.save(_indicated), HttpStatus.OK);
            
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    @PatchMapping("/indicated-movie/{id}")
    public ResponseEntity<IndicatedMovie> patchIndicated(@PathVariable("id") long id, @RequestBody IndicatedMovie indicated) {
       
        Optional<IndicatedMovie> indicatedData = indicatedlRepository.findById(id);
                
        if (indicatedData.isPresent()) {
            IndicatedMovie _indicated = indicatedData.get();
            
            if(indicated.getTitle() != null){
            _indicated.setTitle(indicated.getTitle());
            }
            
            if(indicated.getProducers() != null){
            _indicated.setProducers(indicated.getProducers());
            }
            
            if(indicated.getStudios() != null){
            _indicated.setStudios(indicated.getStudios());
            }
            
            if(indicated.getYear()>0){
            _indicated.setYear(indicated.getYear());    
            }
            
            _indicated.setWinner(indicated.isWinner());
            
            
            
            return new ResponseEntity<>(indicatedlRepository.save(_indicated), HttpStatus.OK);
            
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/indicated-movie/{id}")
    public ResponseEntity<HttpStatus> deleteIndicated(@PathVariable("id") long id) {
        try {
            
            indicatedlRepository.deleteById(id);
            
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/indicated-movie")
    public ResponseEntity<HttpStatus> deleteAllIndicated() {
        try {
            
            indicatedlRepository.deleteAll();
            
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/indicated-movie/winners")
    public ResponseEntity<List<IndicatedMovie>> findByWinners() {
        try {
            
            List<IndicatedMovie> indicated = indicatedlRepository.findByWinner(true);
            
            if (indicated.isEmpty()) {
                
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                
            }
            
            return new ResponseEntity<>(indicated, HttpStatus.OK);
            
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/indicated-movie/producers-analysis")
    public ResponseEntity<Object> findByProducersInterval() {
        try {

            List<WinIntervalResult> results =  indicatedlRepository.findByProducersWithIntervalWin();

            
            Comparator<WinIntervalResult> comparator = Comparator.comparing(WinIntervalResult::getInterval);

            WinIntervalResult resultMin = results.stream().min(comparator).get();
            
            WinIntervalResult resultMax = results.stream().max(comparator).get();
            

            Map<String, Object> map = new HashMap<String, Object>();

            List<Map<String, Object>> min = new ArrayList<Map<String, Object>>();

            List<Map<String, Object>> max = new ArrayList<Map<String, Object>>();

            min.add(resultMin.getMap());

            max.add(resultMax.getMap());
            
            
            for(WinIntervalResult result : results){
                
                if(result.getInterval() == resultMin.getInterval() &&
                   !result.getProducerName().equalsIgnoreCase(resultMin.getProducerName())){
                    min.add(result.getMap());
                }
                
                if(result.getInterval() == resultMax.getInterval() &&
                   !result.getProducerName().equalsIgnoreCase(resultMax.getProducerName())){
                    max.add(result.getMap());
                }
                
            }

            map.put("min", min);
            map.put("max", max);

            return new ResponseEntity<Object>(map, HttpStatus.OK);

        } catch (Exception e) {
            System.out.println("Exception:"+e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
