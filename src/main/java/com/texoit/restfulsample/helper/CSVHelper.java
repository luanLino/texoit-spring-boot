/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.texoit.restfulsample.helper;

import com.texoit.restfulsample.model.IndicatedMovie;
import com.texoit.restfulsample.model.Producer;
import com.texoit.restfulsample.model.Studio;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;

/**
 *
 * @author luan
 */
@Component
public class CSVHelper {
    


    public static List<IndicatedMovie> csvToIndicated(InputStream is) {
        try (
                BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                CSVParser csvParser = new CSVParser(fileReader,
                CSVFormat.EXCEL.withFirstRecordAsHeader().withIgnoreHeaderCase().withDelimiter(';'));
            ) {
            

            List<IndicatedMovie> indicateds = new ArrayList<IndicatedMovie>();
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
                        
            for (CSVRecord csvRecord : csvRecords) {
                
                IndicatedMovie indicated = new IndicatedMovie();
                
                String[] producersName = csvRecord.get("producers").split(",");
                String[] studiosName = csvRecord.get("studios").split(",");
                
                
                Set<Producer> producers = new HashSet<Producer>();
                Set<Studio> studios = new HashSet<Studio>();
                Set<IndicatedMovie> indicatedmovies = new HashSet<IndicatedMovie>();
                
                indicatedmovies.add(indicated);
                
                for(String producerName : producersName){
                    
                   //Producer producer = producerRepository.findByName(producerName.trim());

                   //if(producer==null){
                      Producer producer = new Producer();
                      producer.setName(producerName.trim());
                  //     producerRepository.save(producer);
                  // }

                   producers.add(producer);
                    
                }
                
                for(String studioName : studiosName){
                    
                    //Studio studio = studioRepository.findByName(studioName.trim());

                    //if(studio==null){
                      Studio studio = new Studio();
                       studio.setName(studioName.trim());
                    //   studioRepository.save(studio);
                   //}

                   studios.add(studio);
                }
                
                indicated.setProducers(producers);
                indicated.setStudios(studios);
                indicated.setTitle(csvRecord.get("title"));
                indicated.setYear(CheckHelper.isInteger(csvRecord.get("year"))?Integer.parseInt(csvRecord.get("year")):0);
                
                String worstMovieWinner = csvRecord.get("winner");
                
                
                if(worstMovieWinner == null || worstMovieWinner.isEmpty() || 
                   worstMovieWinner.equalsIgnoreCase("no") || worstMovieWinner.equalsIgnoreCase("not")||
                   worstMovieWinner.equalsIgnoreCase("false") || worstMovieWinner.equalsIgnoreCase("não")){
                   indicated.setWinner(false);
                }else if(worstMovieWinner.equalsIgnoreCase("yes") || worstMovieWinner.equalsIgnoreCase("true") ||
                        worstMovieWinner.equalsIgnoreCase("sim")){
                    indicated.setWinner(true);
                }


                indicateds.add(indicated);
            }
            return indicateds;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        } catch (Exception ex){
            ex.printStackTrace();
            throw new RuntimeException("fail to parse CSV: " + ex.getMessage());
        }
    }
    
    
    public static boolean fileExists(String filePath) {

        File pathChaves = new File(filePath);

        if (pathChaves.exists()) {

            return true;

        } else {

            return false;

        }

    }

}
