/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.texoit.restfulsample.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author luan
 */
@Entity
@DynamicUpdate
@Table(name = "movie")
public class IndicatedMovie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @Column(name = "title")
    private String title;
    
    @ManyToOne
    private IndicatedMovie_Producers indicated_movie;
    
    @ManyToMany//(cascade = CascadeType.ALL)
    private Set<Producer> producers;
    
    @ManyToMany//(cascade = CascadeType.ALL)
    private Set<Studio> studios;
    
    @Column(name = "year")
    private int year;
    
    @Column(name = "winner")
    private boolean winner;

    public IndicatedMovie() {
    }    

    public IndicatedMovie(String title, Set<Producer> producers, Set<Studio> studios, int year, boolean winner) {
        this.title = title;
        this.producers = producers;
        this.studios = studios;
        this.year = year;
        this.winner = winner;
    }

    public long getId() {
        return id;
    }
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Producer> getProducers() {
        return producers;
    }

    public void setProducers(Set<Producer> producers) {
        this.producers = producers;
    }

    public Set<Studio> getStudios() {
        return studios;
    }

    public void setStudios(Set<Studio> studios) {
        this.studios = studios;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean isWinner() {
        return winner;
    }

    public void setWinner(boolean winner) {
        this.winner = winner;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof IndicatedMovie)) {
            return false;
        }
        
        IndicatedMovie otherMovie = (IndicatedMovie) o;

        return (this.hashCode() == otherMovie.hashCode());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 97 * hash + Objects.hashCode(this.title);
        hash = 97 * hash + Objects.hashCode(this.indicated_movie);
        hash = 97 * hash + this.year;
        hash = 97 * hash + (this.winner ? 1 : 0);
        return hash;
    }

}
