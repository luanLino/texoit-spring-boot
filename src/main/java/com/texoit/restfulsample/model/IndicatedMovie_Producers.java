/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.texoit.restfulsample.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author luan
 */
@Entity
@Table(name = "movie_producers")
@IdClass(com.texoit.restfulsample.pojo.CompositKeyMovie_Producers.class)
public class IndicatedMovie_Producers implements Serializable {
    

    //@Column(name = "indicated_movie_id")
    @Id
    @JoinColumn(name = "indicated_movie_id")
    private int indicated_movie_id;
    
    //@Column(name = "producers_id")
    @Id
    @JoinColumn(name = "producers_id")
    private int producers_id;

    public IndicatedMovie_Producers() {
    }    

    public IndicatedMovie_Producers(int indicated_movie_id, int producers_id) {
        this.indicated_movie_id = indicated_movie_id;
        this.producers_id = producers_id;
    }

    /**
     * @return the movie_id
     */
    public int getMovie_id() {
        return indicated_movie_id;
    }

    /**
     * @param movie_id the movie_id to set
     */
    public void setMovie_id(int indicated_movie_id) {
        this.indicated_movie_id = indicated_movie_id;
    }

    /**
     * @return the producers_id
     */
    public int getProducers_id() {
        return producers_id;
    }

    /**
     * @param producers_id the producers_id to set
     */
    public void setProducers_id(int producers_id) {
        this.producers_id = producers_id;
    }


}
