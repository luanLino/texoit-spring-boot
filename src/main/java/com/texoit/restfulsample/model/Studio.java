/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.texoit.restfulsample.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author luan
 */
@Entity
@DynamicUpdate
@Table(name = "studio",uniqueConstraints = 
    @UniqueConstraint(columnNames = "name"))
public class Studio implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "name", nullable = false)
    private String name;
    @ManyToMany
    private Set<IndicatedMovie> indicatedMovies;


    public Studio() {
    }    

    public Studio(String name, Set<IndicatedMovie> indicatedMovies) {
        this.name = name;
        this.indicatedMovies = indicatedMovies;
    }

    public long getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<IndicatedMovie> getIndicatedMovies() {
        return indicatedMovies;
    }

    public void setIndicatedMovies(Set<IndicatedMovie> indicatedMovies) {
        this.indicatedMovies = indicatedMovies;
    }
    


}
