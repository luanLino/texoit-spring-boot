package com.texoit.restfulsample;

import com.texoit.restfulsample.helper.CSVHelper;
import com.texoit.restfulsample.model.IndicatedMovie;
import com.texoit.restfulsample.model.Producer;
import com.texoit.restfulsample.model.Studio;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.texoit.restfulsample.repository.IndicatedMovieRepository;
import com.texoit.restfulsample.repository.ProducerRepository;
import com.texoit.restfulsample.repository.StudioRepository;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class RestfulsampleApplication {

    private static ApplicationContext context;

    private static IndicatedMovieRepository indicatedlRepository;

    private static ProducerRepository producerRepository;

    private static StudioRepository studioRepository;

    public static void loadContext(String[] args) {
        context = SpringApplication.run(RestfulsampleApplication.class, args);
    }

    public static void init(String[] args) {

        indicatedlRepository = (IndicatedMovieRepository) context.getBean("indicatedMovieRepository");

        producerRepository = (ProducerRepository) context.getBean("producerRepository");

        studioRepository = (StudioRepository) context.getBean("studioRepository");

    }

    public static void initializeDatabase() {

        if (!CSVHelper.fileExists("movielist.csv")) {
            System.out.println("Arquivo CSV não encontrado.");
        } else {

            try {

                InputStream csvDataBase = new FileInputStream("movielist.csv");

                List<IndicatedMovie> indicateds = CSVHelper.csvToIndicated(csvDataBase);

                for (IndicatedMovie indicated : indicateds) {

                    Set<Producer> producers = new HashSet<Producer>();

                    Set<Studio> studios = new HashSet<Studio>();

                    for (Producer producer : indicated.getProducers()) {
                        
                        String pName = producer.getName();

                        Producer producerDatabase = producerRepository.findByName(pName);

                        if (producerDatabase == null) {
                            producer = new Producer();
                            producer.setName(pName);
                            producerRepository.save(producer);
                        } else {
                            producer = producerDatabase;
                        }

                        producers.add(producer);

                    }

                    for (Studio studio : indicated.getStudios()) {
                        
                        String sName = studio.getName();

                        Studio studioDatabase = studioRepository.findByName(sName);

                        if (studioDatabase == null) {
                            studio = new Studio();
                            studio.setName(sName);
                            studioRepository.save(studio);
                        } else {
                            studio = studioDatabase;
                        }

                        studios.add(studio);
                    }

                    indicated.setProducers(producers);
                    indicated.setStudios(studios);
                }

                indicatedlRepository.saveAll(indicateds);

            } catch (FileNotFoundException ex) {
                System.out.println("File movielist.csv not found, database not initialized");
                Logger.getLogger(RestfulsampleApplication.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex){
                ex.printStackTrace();
                System.out.println("Fail database not initialized, error:"+ex.getMessage());
            }
        }
    }

    public static ApplicationContext getContext() {
        return context;
    }

    public static void main(String[] args) {

        if (context == null) {
            loadContext(args);
        }

        init(args);

        initializeDatabase();

    }

}
