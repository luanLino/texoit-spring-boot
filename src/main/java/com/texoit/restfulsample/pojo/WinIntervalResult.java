/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.texoit.restfulsample.pojo;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author luan
 */
public class WinIntervalResult {
    
    private String producerName;
    
    private int previousWin;
    
    private int followingWin;
    
    private int intervall;
    
    
    public WinIntervalResult(String producerName, int previousWin, int followingWin, int intervall){
        
        this.producerName = producerName;
        
        this.previousWin = previousWin;
        
        this.followingWin = followingWin;
        
        this.intervall = intervall;
    }
    
    public WinIntervalResult(){
        
    }

    /**
     * @return the producerName
     */
    public String getProducerName() {
        return producerName;
    }

    /**
     * @param producerName the producerName to set
     */
    public void setProducerName(String producerName) {
        this.producerName = producerName;
    }

    /**
     * @return the previousWin
     */
    public int getPreviousWin() {
        return previousWin;
    }

    /**
     * @param previousWin the previousWin to set
     */
    public void setPreviousWin(int previousWin) {
        this.previousWin = previousWin;
    }

    /**
     * @return the followingWin
     */
    public int getFollowingWin() {
        return followingWin;
    }

    /**
     * @param followingWin the followingWin to set
     */
    public void setFollowingWin(int followingWin) {
        this.followingWin = followingWin;
    }

    /**
     * @return the intervall
     */
    public int getInterval() {
        return intervall;
    }

    /**
     * @param interval the intervall to set
     */
    public void setInterval(int interval) {
        this.intervall = interval;
    }
    
    public Map getMap(){
        
       Map<String, Object> map = new HashMap<String, Object>();
       
       map.put("producer", this.producerName);
       map.put("interval", this.intervall);
       map.put("previousWin", this.previousWin);
       map.put("followingWin", this.followingWin);

       return map;
    }
    
}
