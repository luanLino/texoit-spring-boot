/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.texoit.restfulsample.pojo;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author luan
 */
public class CompositKeyMovie_Producers implements Serializable{
    
    private Long indicated_movie_id;
    private Long producers_id;

    public CompositKeyMovie_Producers(Long movie_id, Long producer_id) {
        this.indicated_movie_id = movie_id;
        this.producers_id = producer_id;
    }

    public CompositKeyMovie_Producers() {
    }


    /**
     * @return the indicated_movie_id
     */
    public Long getMovie_id() {
        return indicated_movie_id;
    }

    /**
     * @param movie_id the indicated_movie_id to set
     */
    public void setMovie_id(Long movie_id) {
        this.indicated_movie_id = movie_id;
    }

    /**
     * @return the producers_id
     */
    public Long getProducer_id() {
        return producers_id;
    }

    /**
     * @param producer_id the producers_id to set
     */
    public void setProducer_id(Long producer_id) {
        this.producers_id = producer_id;
    }
    
    

    @Override
    public boolean equals(Object o) {
        if ( this == o ) {
            return true;
        }
        if ( o == null || getClass() != o.getClass() ) {
            return false;
        }
        CompositKeyMovie_Producers pk = (CompositKeyMovie_Producers) o;
        return Objects.equals(getMovie_id(), pk.getMovie_id()) &&
                Objects.equals(getProducer_id(), pk.getProducer_id());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMovie_id(), getProducer_id());
    }
    
    
}
